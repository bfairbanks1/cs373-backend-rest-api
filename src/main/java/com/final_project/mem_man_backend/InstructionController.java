package com.final_project.mem_man_backend;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class InstructionController {

    @CrossOrigin
    @GetMapping("/instructions/{tableName}")
    public ResponseEntity<Object> getInstructions(@PathVariable String tableName) {
       return new ResponseEntity<>(DBDriver.getInstructionsFrontEnd(tableName), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/storage")
    public ResponseEntity<Object> getStorage() {
        if(!DBDriver.getStorageFrontEnd().isEmpty())
            return new ResponseEntity<>(DBDriver.getStorageFrontEnd(), HttpStatus.OK);
        else
            return new ResponseEntity<>("Storage empty", HttpStatus.NOT_FOUND);
    }

    @CrossOrigin
    @GetMapping("/run")
    public ResponseEntity<Object> runOperations() {
        DBDriver.run();
        return new ResponseEntity<>("Operations Performed", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/clear_mem")
    public ResponseEntity<Object> clearMem() {
        DBDriver.clearMemory();
        return new ResponseEntity<>("Memory cleared", HttpStatus.OK);
    }
}
