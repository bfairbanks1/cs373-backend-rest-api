package com.final_project.mem_man_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemManBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemManBackendApplication.class, args);
    }

}
