package com.final_project.mem_man_backend;

public class Instruction {
    private double operand1, operand2;
    private String operation;

    public Instruction(double operand1, double operand2, String operation) {
        this.operand1 = operand1;
        this.operand2 = operand2;
        this.operation = operation;
    }

    public double getOperand1() {
        return operand1;
    }

    public double getOperand2() {
        return operand2;
    }

    public String getOperation() {
        return operation;
    }
}
