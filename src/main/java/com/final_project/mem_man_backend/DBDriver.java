package com.final_project.mem_man_backend;

import java.sql.*;
import java.util.ArrayList;

public class DBDriver {
    private static Statement stmt;
    private static Connection connection;
    private static ArrayList<Double> results = new ArrayList<>();

    private DBDriver() { }

    public static ArrayList<Instruction> getInstructionsFrontEnd(String tableName) {
        return extractInstructions(tableName);
    }

    public static ArrayList<Double> getStorageFrontEnd() {
        return extractStorage();
    }

    /**
     * This
     * connects
     * the
     * database
     */
    public static void connect() {
        try {
            //This url will have to be modified for your local machine
            connection = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Brandon\\IdeaProjects\\mem_man_backend\\computer_system.db");
            if(connection != null) {
                System.out.println("Opened database successfully");
                stmt = connection.createStatement();
            }
            else
                System.out.println("Something went wrong, getConnection passed but connection is null");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.out.println("Could not connect, try again");
        }
    }

    /**
     * Methods
     * below
     * represent
     * the
     * functioning
     * of
     * a
     * memory manager
     */
    public static void run() {
        try {
            loadMemory();
            clearProgramInstructions();
            extractInstructionsInMem();
            writeResultsToStorage();
            //clearMemory();
        }
        catch (Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage() + "run method");
        }
    }

    public static void loadMemory() {
        String cmd = "UPDATE memory " +
                "SET (operand_1, operand_2, operation) = (SELECT program_instructions.operand_1, program_instructions.operand_2, program_instructions.operation " +
                    "FROM program_instructions " +
                    "WHERE program_instructions.command_id = memory.mem_addr ) " +
                    "WHERE " +
                        "EXISTS ( " +
                            "SELECT * " +
                            "FROM program_instructions " +
                            "WHERE program_instructions.command_id = memory.mem_addr)";
        try {
            connect();
            stmt.execute(cmd);
            System.out.println("Memory loaded");
            closeDB();
        }
        catch (Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage() + "loadMem method");
        }
    }

    public static void clearProgramInstructions() {
        String cmd = "DELETE FROM program_instructions " +
                "WHERE EXISTS (SELECT * FROM memory " +
                "WHERE program_instructions.command_id = memory.mem_addr AND " +
                "program_instructions.operand_1 = memory.operand_1 AND " +
                "program_instructions.operand_2 = memory.operand_2 AND " +
                "program_instructions.operation = memory.operation)",
        cmd2 = "UPDATE program_instructions SET command_id = ROWID-8";
        try{
            connect();
            stmt.execute(cmd);
            System.out.println("Program instructions added to memory removed from table");
            stmt.execute(cmd2);
            System.out.println("Program instructions properly indexed");
            closeDB();
        }
        catch (Exception e){
            System.err.println(e.getClass().getName() + ": " + e.getMessage() + "loadMem method");
        }
    }

    public static void extractInstructionsInMem() {
        String cmd = "SELECT m.operand_1, m.operand_2, m.operation FROM memory m WHERE m.operand_1 NOT NULL ";
        try {
            connect();
            ResultSet rs =  stmt.executeQuery(cmd);
            if(rs != null){
                while(rs.next()){
                    double operandOne = rs.getDouble("operand_1");
                    double operandTwo = rs.getDouble("operand_2");
                    String operation = rs.getString("operation");
                    executeInstructions(operandOne, operandTwo, operation);
                }
            }
            System.out.println("Operations performed");
            closeDB();
        }
        catch (Exception e){
            System.out.println(e.getClass().getName() + ": " + e.getMessage() + "execute method");
        }
    }

    private static void executeInstructions(double operandOne, double operandTwo, String operation) {
        switch (operation) {
            case "ADD":
                results.add(operandOne + operandTwo);
                break;
            case "SUB":
                results.add(operandOne - operandTwo);
                break;
            case "MUL":
                results.add(operandOne * operandTwo);
                break;
            case "DIV":
                results.add(operandOne/operandTwo);
                break;
            default:
                System.out.println("Invalid operation");
        }
    }

    public static void clearMemory() {
        String cmd = "UPDATE memory SET operand_1=null, operand_2=null, operation=null";

        try {
            connect();
            stmt.execute(cmd);
            System.out.println("Memory cleared");
            closeDB();
        }
        catch (Exception e){
            System.out.println(e.getMessage() + "clear memory fucked up");
        }
    }

    private static void writeResultsToStorage() {
        try {
            connect();
            for (double result : results) {
                String cmd = "INSERT INTO storage(result) VALUES(" + result + ")";
                stmt.execute(cmd);
            }
            results.removeAll(results);
            closeDB();
        } catch (Exception e) {
            System.out.println(e.getClass().getName() + ": " + e.getMessage() + "execute method");
        }
    }

    /**
     * These
     * methods
     * do
     * the
     * work
     * for
     * InstructionController
     */
    public static ArrayList<Instruction> extractInstructions(String tableName){
        ArrayList<Instruction> instructions = new ArrayList<>();
        connect();
        ResultSet rs;
        try {
            rs = stmt.executeQuery("SELECT * FROM " + tableName);
            if(rs != null) {
                while (rs.next()) {
                    instructions.add(new Instruction(rs.getDouble("operand_1"), rs.getDouble("operand_2"), rs.getString("operation")));
                }
            connection.close();
            }
        } catch (Exception e) {
            System.out.println(e.getClass().getName() + ": " + e.getMessage() + "execute method");
        }
        return instructions;
    }


    public static ArrayList<Double> extractStorage() {
        ArrayList<Double> storage = new ArrayList<>();
        connect();
        ResultSet rs;
        try {
            rs = stmt.executeQuery("SELECT * FROM storage");
            if(rs != null) {
                while (rs.next()) {
                    storage.add(rs.getDouble("result"));
                }
            }
            closeDB();
        } catch (Exception e) {
            System.out.println(e.getClass().getName() + ": " + e.getMessage() + "execute method");
        }
        return storage;
    }

    /**
     * This
     * closes
     * the
     * database
     */
    public static void closeDB() {
        try{
            connection.close();
            System.out.println("Database closed successfully");
        } catch (Exception e){
            e.getStackTrace();
        }
    }
}
